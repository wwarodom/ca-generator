#!/bin/bash -x

###########################################
#	PaOCLOUD ACADEMY Certificate Generator
#	Payungsak Klinchampa
#	pao@paocloud.in.th
###########################################

set -e

for C in `echo root-ca intermediate-ca`; do

  mkdir $C
  cd $C
  mkdir certs crl newcerts private
  cd ..

  echo 01 > $C/serial
  touch $C/index.txt $C/index.txt.attr

done

## Genberate and sign Root CA.
openssl genrsa -out root-ca/private/ca.key 2048
openssl req -config openssl-rootca.cnf -new -x509 -days 3650 -key root-ca/private/ca.key -sha256 -extensions v3_req -out root-ca/certs/ca.crt -subj '/C=TH/postalCode=86110/ST=Chumphon/L=Langsuan/O=PaOCLOUD/OU=Certificate Department/CN=PaOCLOUD Root CA/emailAddress=support@paocloud.academy'

## Genberate and sign Intermediate CA.
openssl genrsa -out intermediate-ca/private/intermediate.key 2048
openssl req -config openssl-inter.cnf -sha256 -new -key intermediate-ca/private/intermediate.key -out intermediate-ca/certs/intermediate.csr -subj '/C=TH/postalCode=86110/ST=Chumphon/L=Langsuan/O=PaOCLOUD/OU=Certificate Department/CN=PaOCLOUD Intermediate CA/emailAddress=support@paocloud.academy'
openssl ca -batch -config openssl-rootca.cnf -keyfile root-ca/private/ca.key -cert root-ca/certs/ca.crt -extensions v3_req -notext -md sha256 -in intermediate-ca/certs/intermediate.csr -out intermediate-ca/certs/intermediate.crt

mkdir cert-out

## Genberate and sign Certificate.
openssl req -new -config openssl-server.cnf -keyout cert-out/test.payungsakpk.xyz.key -out cert-out/test.payungsakpk.xyz.request -days 365 -nodes -newkey rsa:2048 -subj '/C=TH/postalCode=86110/ST=Chumphon/L=Langsuan/O=PaOCLOUD/OU=Certificate Department/businessCategory=Private Organization/serialNumber=00001/CN=test.payungsakpk.xyz/emailAddress=support@paocloud.academy'
openssl ca -batch -extensions v3_req -config openssl-server.cnf -keyfile intermediate-ca/private/intermediate.key -cert intermediate-ca/certs/intermediate.crt -out cert-out/test.payungsakpk.xyz.crt -infiles cert-out/test.payungsakpk.xyz.request
